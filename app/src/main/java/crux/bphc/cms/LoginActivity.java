package crux.bphc.cms;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import androidx.annotation.AnimRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import app.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import helper.*;
import okhttp3.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.Random;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.username)
    EditText usernameEditText;

    @BindView(R.id.password)
    EditText passwordEditText;

    @BindView(R.id.url)
    TextInputEditText urlEditText;

    @BindView(R.id.server)
    RelativeLayout urlLayout;

    @BindView(R.id.credentials)
    RelativeLayout credentialsLayout;

    @BindView(R.id.browserAuth)
    LinearLayout browserLayout;

    @BindView(R.id.connect)
    Button urlButton;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.browserOpenCredentials)
    Button browserOpenCredentials;

    private MoodleServices moodleServices;
    private UserAccount userAccount;

    private String launchUrl;

    private Drawable openLockDrawable;
    boolean closedLockDisplayed = false;

    private State state = State.SERVER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.LoginTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        userAccount = new UserAccount(this);
        checkLoggedIn();

        passwordEditText.setOnKeyListener((view, i, keyEvent) -> {
                    if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                        onClickLogin();
                        return true;
                    } else return false;
                }
        );

        urlEditText.setOnKeyListener((view, i, keyEvent) -> {
                    if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                        onClickCheckURL();
                        return true;
                    } else return false;
                }
        );
    }

    @OnTextChanged(R.id.url)
    void onTextChanged(CharSequence text) {
        if (text.toString().toLowerCase().startsWith("http:")) {

            if (openLockDrawable == null) {
                openLockDrawable = getDrawable(R.drawable.ic_lock_open);
                openLockDrawable.setBounds(0, 0,
                        openLockDrawable.getIntrinsicWidth(), openLockDrawable.getIntrinsicHeight());
            }

            urlEditText.setError(getString(R.string.warning_insecure), openLockDrawable);
            closedLockDisplayed = false;
        } else if (text.toString().isEmpty()) {
            urlEditText.setCompoundDrawables(null, null, null, null);
            closedLockDisplayed = false;
        } else {
            if (!closedLockDisplayed) {
                urlEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_lock, 0);
                closedLockDisplayed = true;
            }
        }
    }

    @OnClick(R.id.connect)
    void onClickCheckURL() {

        if (state != State.SERVER) return;

        String url = urlEditText.getText().toString();
        if (url.length() < 1) {
            urlEditText.setError(getString(R.string.login_error_no_url));
            closedLockDisplayed = false;
            return;
        }
        if (!url.startsWith("http")) {
            url = "https://" + url;
        }
        if (!url.substring(url.length() - 1).equals("/")) {
            url = url + "/";
        }


        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String json = "[{\"index\":0,\"methodname\":\"tool_mobile_get_public_config\",\"args\":{}}]";
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url + "lib/ajax/service.php?info=tool_mobile_get_public_config")
                .post(body)
                .build();

        OkHttpClient client = new OkHttpClient();
        String finalUrl = url;
        client.newCall(request).enqueue(new okhttp3.Callback() {

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                Log.d("URL", call.request().url().toString());

                state = State.SERVER;

                runOnUiThread(() -> {
                    urlEditText.setError(getString(R.string.login_error_no_connection));
                    closedLockDisplayed = false;
                });

                e.printStackTrace();
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                String token = response.body().string();
                Log.d("token", token);
                if (token.contains("wwwroot")) {
                    token = token.substring(1, token.length() - 1);
                    JsonParser parser = new JsonParser();
                    JsonObject jsonObj = parser.parse(token).getAsJsonObject();
                    String seitenname = jsonObj.getAsJsonObject("data").get("sitename").getAsString();
                    String logintype = jsonObj.getAsJsonObject("data").get("typeoflogin").getAsString();
                    launchUrl = jsonObj.getAsJsonObject("data").get("launchurl").getAsString();
                    Log.d("Logintype", logintype);
                    Log.d("Seitenname", seitenname);
                    userAccount.setSitename(seitenname);
                    userAccount.setURL(finalUrl);
                    Constants.API_URL = finalUrl;

                    boolean authInBrowser = !logintype.equals("1");

                    runOnUiThread(() -> {

                        setTitle(seitenname);
                        title.setText(getString(R.string.title_login_name, seitenname));

                        if (authInBrowser && getCurrentFocus() != null) {
                            InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        }

                        animate(urlLayout,
                                authInBrowser ? browserLayout : credentialsLayout,
                                true,
                                authInBrowser ? State.BROWSER : State.CREDENTIALS
                        );
                    });

                } else {
                    runOnUiThread(() -> {
                        urlEditText.setError(getString(R.string.login_error_no_moodle));
                        getAlertDialog(R.string.login_error_no_moodle_detail).show();
                        closedLockDisplayed = false;
                    });
                    Log.w("Seitenname", "No moodle found at URL");
                    state = State.SERVER;
                }
            }
        });
        state = State.QUERYING;
    }


    @OnClick(R.id.login)
    void onClickLogin() {

        if (state != State.CREDENTIALS && state != State.CREDENTIALS_FORCE) return;

        State initialState = state;

        if (moodleServices == null) {
            Retrofit retrofit = APIClient.getRetrofitInstance();
            moodleServices = retrofit.create(MoodleServices.class);
        }


        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        boolean error = false;

        if (username.length() < 1) {
            usernameEditText.setError(getString(R.string.provide_username));
            error = true;
        }
        if (password.length() < 1) {
            passwordEditText.setError(getString(R.string.provide_password));
            error = true;
        }
        if (error) return;

        ProgressDialog progressDialog = new ProgressDialog(this, R.style.LoginTheme_AlertDialog);

        progressDialog.show();
        progressDialog.setMessage(getString(R.string.login_progress_authenticating));

        Call<ResponseBody> call = moodleServices.getTokenByLogin(username, password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                String responseString = "";
                try {
                    responseString = response.body().string();
                } catch (IOException | NullPointerException e) {
                    getAlertDialog(R.string.unknown_networkerror).show();
                    e.printStackTrace();
                    state = initialState;
                    return;
                }

                UserToken userToken = new Gson().fromJson(responseString, UserToken.class);
                final String token = userToken.getToken();
                if (token == null || token.length() < 25) {
                    getAlertDialog(R.string.login_error_credentials).show();
                    progressDialog.dismiss();
                    state = initialState;
                    return;
                }

                new UserDetailQueryRunnable(LoginActivity.this, progressDialog, moodleServices, userAccount, token).run();

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
            }
        });

        state = State.QUERYING;
    }

    @OnClick(R.id.browserOpen)
    void onClickBrowserOpen() {

        if (state != State.BROWSER) return;

        Random random = new Random();
        String rand = String.valueOf(random.nextInt(1000));
        String url = launchUrl + "?service=local_mobile&passport=" + rand + "&urlscheme=moodledirect";

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.browserOpenCredentials)
    void onClickBrowserOpenCredentials() {

        if (state != State.BROWSER) return;

        animate(browserLayout, credentialsLayout, true, State.CREDENTIALS_FORCE);
    }

    private void checkLoggedIn() {
        if (userAccount.isLoggedIn()) {
            Intent intent = new Intent(this, MainActivity.class);
            if (getIntent().getParcelableExtra("path") != null) {
                intent.putExtra(
                        "path",
                        getIntent().getParcelableExtra("path").toString()
                );
            }
            startActivity(intent);
            finish();
        }
    }

    private AlertDialog getAlertDialog(@StringRes int text) {
        return new AlertDialog.Builder(this, R.style.LoginTheme_AlertDialog)
                .setMessage(text)
                .create();
    }

    private enum State {
        SERVER, CREDENTIALS, BROWSER, CREDENTIALS_FORCE, QUERYING, ANIMATING
    }

    @Override
    public void onBackPressed() {
        switch (state) {
            case SERVER:
                super.onBackPressed();
                return;
            case CREDENTIALS:

                animate(credentialsLayout, urlLayout, false, State.SERVER);

                title.setText(R.string.title_login);

                moodleServices = null;
                APIClient.clearRetrofitInstance();

                return;
            case BROWSER:
                animate(browserLayout, urlLayout, false, State.SERVER);

                title.setText(R.string.title_login);

                moodleServices = null;
                APIClient.clearRetrofitInstance();

                return;
            case CREDENTIALS_FORCE:

                animate(credentialsLayout, browserLayout, false, State.BROWSER);
                return;
        }
    }

    private void animate(View from, View to, boolean rightToLeft, State lastState) {

        state = State.ANIMATING;

        Animation slideOutAnimation = AnimationUtils.loadAnimation(LoginActivity.this,
                rightToLeft ? R.anim.slide_out_left : R.anim.slide_out_right
        );

        slideOutAnimation.setAnimationListener(
                new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        from.setVisibility(View.GONE);

                        Animation followUpAnimation = AnimationUtils.loadAnimation(LoginActivity.this,
                                rightToLeft ? R.anim.slide_in_right : R.anim.slide_in_left
                        );

                        followUpAnimation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                to.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                state = lastState;
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        to.startAnimation(followUpAnimation);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                }
        );
        from.startAnimation(slideOutAnimation);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Uri uri = intent.getData();

        String tokenstring = uri.toString();
        Log.d("Intent-Response", tokenstring);

        UserAccount userAccount = new UserAccount(this);

        // Prepare String to just get base64
        tokenstring = tokenstring.replace("moodledirect://token=", "");
        byte[] decodeValue = Base64.decode(tokenstring, Base64.DEFAULT);
        tokenstring = new String(decodeValue);
        Log.d("TEST", "decodeValue = " + tokenstring);

        String token = tokenstring.split(":::")[1];

        Retrofit retrofit = APIClient.getRetrofitInstance();
        MoodleServices moodleServices = retrofit.create(MoodleServices.class);


        new UserDetailQueryRunnable(LoginActivity.this, null, moodleServices, userAccount, token)
                .run();

    }
}
